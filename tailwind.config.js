module.exports = {
  purge: [
    '*.html', '*.htm', '*.blade.php'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        xs: '480px',
        '3xl': '1800px'
      },
      colors:{
        'gold-light': '#F0E68C',
        'gold-dark': '#996515'
      },
      fontFamily: {
        'lobster': ['Lobster']
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
